section .data

%define SYSCALL_FOR_EXIT 60  
%define NULL_TERMINATOR 0x00 
%define TAB 0x9  
%define SPACE 0x20 
%define NEW_LINE `\n`
%define MINUS `-`
%define PLUS `+`

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_FOR_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi + rax], NULL_TERMINATOR
        je .exit
        inc rax
        jmp .loop
    .exit:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns .plus

    .minus:
        push rdi
	    mov rdi, MINUS
	    call print_char
	    pop rdi
	    neg rdi
    .plus:
	    jmp print_uint
	    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rcx, rcx
    mov rax, rdi
    mov r9, 20
    mov r10, 10
    sub rsp, 32
    mov byte [rsp + r9], 0
    mov rax, rdi
  
    .accumulate:
        xor rdx, rdx
        div r10
        add rdx, '0'
        dec r9
	    mov byte [rsp+r9], dl
	    inc rcx
 	    test rax, rax
        je .print_number
        jmp .accumulate
    .print_number:
        lea rdi, [rsp+r9]
        call print_string
        add rsp, 32
	    ret

;Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx

    .check_on_equals:
        mov r9b, byte [rdi + rcx]
        cmp r9b, byte [rsi + rcx]
        jne .not_same
	test r9, r9
	jz .same
	inc rcx
	jmp .check_on_equals
    .same:
        mov rax, 1
        ret
    .not_same:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    
    test rax, rax
    jz .null_found
    mov al, byte [rsp]
    jmp .end

    .null_found:
    	xor rax, rax
    .end:
    	pop rdi
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14

    .skip_spaces:
        call read_char
        cmp rax, TAB
        je .skip_spaces
        cmp rax, NEW_LINE
        je .skip_spaces
        cmp rax, SPACE
        je .skip_spaces
    .read:
        cmp r14, r13
        je .error
        cmp al, NULL_TERMINATOR
	je .end
        cmp al, TAB
	je .end
        cmp al, NEW_LINE
	je .end
        cmp al, SPACE
	je .end

        mov byte [r12 + r14], al
        inc r14
        call read_char
        jmp .read

    .end:
        mov byte [r12 + r14], NULL_TERMINATOR
        mov rax, r12
	    mov rdx, r14
    .ending:
        pop r14
        pop r13
        pop r12
        ret

    .error:
        xor rax, rax
        jmp .ending
    


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r8, r8
    xor r9, r9

    .check_number:
        mov r9b, byte [rdi + r8]
        test r9b, r9b
        je .exit
        cmp r9b, '0'
        jl .exit
        cmp r9b, '9'
        jg .exit

    .parse:
        inc r8
        sub r9b, '0'
        mul r10
        add rax, r9
        jmp .check_number

    .exit:
        mov rdx, r8
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov r9b, [rdi]
    cmp r9b, MINUS
    jne .plus

    .sign:
    	push r9
	    inc rdi
	    push rdi
	    call parse_uint
	    pop rdi
	    pop r10
	    cmp r10, PLUS
	    je .exit
	    neg rax

    .exit:
    	inc rdx
	    ret

    .plus:
    	cmp r9b, PLUS
    	je .sign
    	push rdi 
    	call parse_uint
    	pop rdi
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdx
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    pop rdx

    cmp rax, rdx
    jg .failure
    xor rax, rax

    .fill_buffer:
        mov r9b, byte [rdi + rax]
        mov byte [rsi + rax], r9b
        test r9b, r9b
        je .successful
        inc rax
        jmp .fill_buffer

    .failure:
        xor rax, rax
    .successful:
        ret

